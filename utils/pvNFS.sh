#!/bin/bash

set -x

server=$1
project=$2
pvname=$3
capacity=$4

templatename=$( echo -n "${project}-${pvname}" | tr '[:upper:]' '[:lower:]' )

if [ "$#" -ne 4 ]; then
    echo "Parameters: server project pvname capacity"
    exit 1
fi

ssh $server -l root "mkdir -p /nfs_ocp/$project/$pvname/;exit 0"
ssh $server -l root "chown -R nfsnobody:nfsnobody /nfs_ocp/$project"
ssh $server -l root "chmod -R 777 /nfs_ocp/$project"
ssh $server -l root "echo '/nfs_ocp/$project/$pvname *(rw,async,all_squash)' >>/etc/exports; mv /etc/exports /tmp; sort -u /tmp/exports >/etc/exports"
ssh $server -l root "cat /etc/exports"
ssh $server -l root "systemctl restart nfs-server"

cat <<EOYAML | oc create -f -
apiVersion: v1
kind: PersistentVolume
metadata:
  name: $templatename
spec:
  capacity:
    storage: $capacity
  accessModes:
    - ReadWriteOnce
    - ReadWriteMany
  nfs: 
    path: /nfs_ocp/$project/$pvname
    server: $server
  persistentVolumeReclaimPolicy: Retain
EOYAML

